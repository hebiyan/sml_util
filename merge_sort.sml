
fun merge pred key llst rrst =
  case (llst, rrst) of
      ([], _) => rrst
    | (_, []) => llst
    | ((x::xs), (y::ys)) => if pred ((key x), (key y)) = LESS then
                              x :: merge pred key xs rrst
                          else
                              y :: merge pred key llst ys;

fun merge_sort pred key lst =
  case lst of
      [] => []
    | [a] => [a]
    | _ => let
        val len = List.length lst
        val half = len div 2
        val head = List.take (lst, half)
        val tail = List.drop (lst, half)
    in 
        merge pred key
              (merge_sort pred key head) (merge_sort pred key tail)
    end;
